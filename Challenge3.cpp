 #include<iostream>
#include<string>
#include<bits/stdc++.h> 
#include <string.h>
using namespace std;

void palindromeStringH(char str[],int len);
void palindromeString(string str);
void palindrome_test(char str[],int len,string str2);

int main() {
	const int strLen=100;
	char cstr[strLen];
//	cin>>cstr;
	cin.getline(cstr,strLen);
//	palindromeStringH(cstr,strLen);
	string str;
	str = cstr;
//	palindromeString(str);
	palindrome_test(cstr,strLen,str);
	return 0;
}

void palindrome_test(char str[],int len,string str2){
	palindromeStringH(str,len);
	palindromeString(str2);
}

void palindromeStringH(char str[],int len){
	char palin[len];
	strcpy(palin,str);
	strrev(str);
	if(strcmp(str, palin)==0) cout<<"IS PALINDEROME CHAR ARRAY"<<endl;
	else cout<<"IS NOT PALINDORME CHAR ARRAY"<<endl;
};

void palindromeString(string str){
	string copyString = str;
	reverse(str.begin(),str.end());
	if (str.compare(copyString) == 0) cout<<"IS PALINDEROME STRING"<<endl;
	else cout<<"IS NOT PALINDROME STRING"<<endl;
};
